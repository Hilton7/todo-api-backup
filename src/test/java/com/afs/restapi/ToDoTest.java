package com.afs.restapi;

import com.afs.restapi.entity.ToDo;
import com.afs.restapi.repository.ToDoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class ToDoTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ToDoRepository toDoRepository;

    @Test
    void should_get_todo_when_given_id() throws Exception {
        ToDo todo = new ToDo("sada", false);
        toDoRepository.save(todo);
        mockMvc.perform(get("/todos/" + todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));

        Optional<ToDo> optionalTodo = Optional.ofNullable(toDoRepository.findById(todo.getId())).get();
        assertTrue(optionalTodo.isPresent());
        Assertions.assertEquals(todo.getId(), optionalTodo.get().getId());
        Assertions.assertEquals(todo.getText(), optionalTodo.get().getText());
        Assertions.assertEquals(todo.getDone(), optionalTodo.get().getDone());
    }

    @Test
    void should_get_todos() throws Exception {
        ToDo todo = new ToDo("小白号码", false);
        toDoRepository.save(todo);
        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data[0].done").value(todo.getDone()));
    }


    @Test
    void should_create_todo() throws Exception {
        ToDo todo = new ToDo("eeeee", false);

        ObjectMapper objectMapper = new ObjectMapper();
        String todoJson = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoJson))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.done").value(false));
    }

    @Test
    void should_delete_todo() throws Exception {
        ToDo todo = new ToDo("小白号码", false);
        toDoRepository.save(todo);

        mockMvc.perform(delete("/todos/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertFalse(toDoRepository.findById(todo.getId()).isPresent());
    }


    @Test
    void should_update_todo() throws Exception {

        ToDo todo = new ToDo("xiaobai", false);
        toDoRepository.save(todo);
        ToDo updatingTodo = new ToDo("xiaoming", false);

        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(updatingTodo);

        mockMvc.perform(put("/todos/{id}", todo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<ToDo> optionalTodo = toDoRepository.findById(todo.getId());
        assertTrue(optionalTodo.isPresent());
        ToDo updatedTodo = optionalTodo.get();
        Assertions.assertEquals(updatedTodo.getId(), todo.getId());
        Assertions.assertEquals(updatedTodo.getDone(), updatingTodo.getDone());
    }

    @Test
    void should_throw_NullObjException_when_get_by_id_given_id() throws Exception {
        mockMvc.perform(get("/todos/100"))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("no this object"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("404"));
    }
}