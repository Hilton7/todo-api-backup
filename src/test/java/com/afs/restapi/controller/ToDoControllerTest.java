package com.afs.restapi.controller;

import com.afs.restapi.entity.ToDo;
import com.afs.restapi.service.ToDoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@AutoConfigureMockMvc
@SpringBootTest
public class ToDoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ToDoService toDoService;
    @Autowired
    ObjectMapper mapper;

    @Test
    void should_call_service_when_perform_get_todos_given_() throws Exception {
        //given
        ToDo toDo = new ToDo("test", false);
        //when
        mockMvc.perform(get("/todos"));
        verify(toDoService).getAllToDo();
    }

    @Test
    void should_call_service_when_perform_get_todo_given_id() throws Exception {
        //given
        ToDo toDo = new ToDo("test", false);
        //when
        mockMvc.perform(get("/todos/1"));
        verify(toDoService).getToDoById(1l);
    }

}
