package com.afs.restapi.controller;

import com.afs.restapi.entity.ResponseObj;
import com.afs.restapi.entity.ToDo;
import com.afs.restapi.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/todos")
public class TodoController {
    @Autowired
    private ToDoService todoService;

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseObj<ToDo> getToDoById(@PathVariable("id") Long id) {
        ToDo toDoById = todoService.getToDoById(id);
        return ResponseObj.OK(toDoById);
    }

    @GetMapping("")
    public ResponseObj<List<ToDo>> getAllToDos() {
        List<ToDo> allToDos = todoService.getAllToDo();
        return ResponseObj.OK(allToDos);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public ResponseObj<ToDo> saveTodo(@RequestBody ToDo toDo) {
        ToDo toDoObj = todoService.saveTodo(toDo);
        return ResponseObj.OK(toDoObj);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{id}")
    public ResponseObj<ToDo> updateTodo(@PathVariable("id") Long id, @RequestBody ToDo toDo) {
        ToDo toDoObj = todoService.updateToDoById(id, toDo);
        return ResponseObj.OK(toDoObj);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public ResponseObj<ToDo> deleteTodo(@PathVariable("id") Long id) {
        ToDo toDoObj = todoService.deleteToDoById(id);
        return ResponseObj.OK(toDoObj);
    }

}
