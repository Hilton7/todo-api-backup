package com.afs.restapi.repository;

import com.afs.restapi.entity.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository(value = "toDoRepository")
public interface ToDoRepository extends JpaRepository<ToDo, Long> {

}
