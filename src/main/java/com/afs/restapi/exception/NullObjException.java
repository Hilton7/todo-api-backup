package com.afs.restapi.exception;

public class NullObjException extends RuntimeException {
    public NullObjException(String message) {
        super(message);
    }
}
