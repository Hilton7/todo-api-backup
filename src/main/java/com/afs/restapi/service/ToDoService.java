package com.afs.restapi.service;

import com.afs.restapi.entity.ToDo;
import com.afs.restapi.exception.NullObjException;
import com.afs.restapi.repository.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ToDoService {
    @Autowired
    private ToDoRepository toDoRepository;

    public ToDo getToDoById(Long id) {
        Optional<ToDo> toDo = toDoRepository.findById(id);
        if (Objects.isNull(toDo) || toDo.isEmpty())
            throw new NullObjException("no this object");
        return toDo.get();
    }

    public List<ToDo> getAllToDo() {
        List<ToDo> toDos = toDoRepository.findAll();
        return toDos;
    }


    public ToDo saveTodo(ToDo toDo) {
        ToDo save = toDoRepository.save(toDo);
        return save;
    }


    public ToDo updateToDoById(Long id, ToDo toDo) {
        ToDo toDoById = getToDoById(id);
        if (toDo.getDone() != null)
            toDoById.setDone(toDo.getDone());
        if (toDo.getText() != null)
            toDoById.setText(toDo.getText());
        toDoRepository.save(toDoById);
        return toDoById;
    }

    public ToDo deleteToDoById(Long id) {
        ToDo toDoById = getToDoById(id);
        toDoRepository.delete(toDoById);
        return toDoById;
    }
}
