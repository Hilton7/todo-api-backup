package com.afs.restapi.entity;

public enum ResponseStatus {
    SUCCESS("200", "Success");
    private String code;
    private String message;

    ResponseStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
