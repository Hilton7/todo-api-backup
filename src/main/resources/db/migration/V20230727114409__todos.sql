create table todos(
    id bigint auto_increment primary KEY,
    text varchar(255),
    done bool
)