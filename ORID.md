# **What did we learn today? What activites did you do? What scenes have impressed you**

Today I learned about front-end and back-end cross-domain joint debugging and cross-domain solutions, among which I was deeply impressed by cross-domain solutions because I solved them myself.

# **Pleas use one word to express your feelings about today's class.**

useful.

# **What do you think about this? What was the most meaningful aspect of this activity?**

Although the things I learned today are not as much as yesterday, they are all quite useful, and cross-domain solutions are the most useful I think.

# **Where do you most want to apply what you have learned today? What changes will you make?**

I will use what I learned today in my future front-end separation projects.
